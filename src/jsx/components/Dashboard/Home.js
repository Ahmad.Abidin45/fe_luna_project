import React,{useContext, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import loadable from "@loadable/component";
import pMinDelay from "p-min-delay";
import {Dropdown} from 'react-bootstrap';

//Import Components
import { ThemeContext } from "../../../context/ThemeContext";
import DropDownBlog from './DropDownBlog';
import CourseBlog from './Dashboard/CourseBlog';
import CalendarBlog from './Dashboard/CalendarBlog';

//images
import Educat from './../../../images/egucation-girl.png';
import Calpng from './../../../images/vector/calpng.png';
import Book from './../../../images/vector/book.png';
import CalenderHome from './Dashboard/CalenderHome';

import { Fragment } from 'react'

/// Page Title
import PageTitle from '../../layouts/PageTitle'

/// Images
import img1 from '../../../images/big/img1.jpg'
import img2 from '../../../images/big/img2.jpg'
import img3 from '../../../images/big/img3.jpg'
import img4 from '../../../images/big/img4.jpg'
import img5 from '../../../images/big/img5.jpg'
import img6 from '../../../images/big/img6.jpg'
import img7 from '../../../images/big/img7.jpg'

/// Bootstrap
import { Row, Col, Card, Carousel } from 'react-bootstrap'

/// carousel data
const carousel1 = [img1, img2, img3]
const carousel2 = [
  { img: img2, text: 'Frist' },
  { img: img3, text: 'Second' },
  { img: img4, text: 'Third' },
]
const carousel3 = [img3, img4, img5]
//const carousel4 = [img4, img5, img6]
const carousel5 = [
  { img: img5, text: 'Frist' },
  { img: img6, text: 'Second' },
  { img: img7, text: 'Third' },
]


const LearningActivityChart = loadable(() =>
	pMinDelay(import("./Dashboard/LearningActivityChart"), 1000)
);
const ScoreActivityChart = loadable(() =>
	pMinDelay(import("./Dashboard/ScoreActivityChart"), 1000)
);
const ProgressChart = loadable(() =>
	pMinDelay(import("./Dashboard/ProgressChart"), 1000)
);

const Home = () => {
	const { changeBackground } = useContext(ThemeContext);
	useEffect(() => {
		changeBackground({ value: "light", label: "Light" });
	}, []);
	const [dropSelect, setDropSelect] = useState('This Month');
	return(
		<>
		{/* <div className='row'> */}
		<div className="row">
				<div className="col-xl-12 col-xxl-12">
							<div className='card'>
								<div className='card-body'>
					<div className="row">
						<div className="col-xl-4 col-xxl-6">
								<div className="text-center">
							<h5><Link to={"./courses"} >Course</Link></h5>
							</div>
							
							
						</div>
						<div className="col-xl-4 col-xxl-6">
							<div className="text-center">
							<h5><Link to={"./SMiLe_Certification"}>SMiLe Certification</Link></h5>
							</div>
						</div>
						<div className="col-xl-4 col-xxl-6">
							<div className="text-center">
							<h5>Digital Content Library</h5>
							</div>
						</div>
						</div>
							</div>
					</div>
				</div>
		{/* </div> */}
		</div>
			<div className="row">
				<div className="col-xl-6 col-xxl-12">
					<div className="row">
						<div className="col-xl-12 col-xxl-6">
							<div className="">
							<CourseBlog />
							</div>
						</div>
					</div>
				</div>
				<div className="col-xl-6 col-xxl-12">
					<div className="row">
						<div className="col-xl-12 col-xxl-6">
							<div className="">
									<CalenderHome/>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<div className='row'>
			<Col xl={12}>
          <Card>
            <Card.Body className='p-4'>
				<div className='row'>
					<div className='col'><h4 className='card-intro-title mb-4'>Article</h4></div>
					<div className='col'><h4 className='card-intro-title mb-4 text-end'><Link to={"./courses"} className="btn btn-primary btn-sm">View all</Link></h4></div>
				</div>
              <Carousel>
						<Carousel.Item kei={0}>
				<div className='row'>
					<div className='col-xl-4'>
							<img
							className='d-block w-100'
							src={img1}
							/>
							<h5 className='text-center'>Description</h5>
					</div>
					<div className='col-xl-4'>
							<img
							className='d-block w-100'
							src={img2}
							/>
							<h5 className='text-center'>Description</h5>
					</div>
					<div className='col-xl-4'>
							<img
							className='d-block w-100'
							src={img3}
							/>
							<h5 className='text-center'>Description</h5>
					</div>
				</div>
						</Carousel.Item>
						<Carousel.Item kei={1}>
				<div className='row'>
					<div className='col-xl-4'>
							<img
							className='d-block w-100'
							src={img4}
							/>
							<h5 className='text-center'>Description</h5>
					</div>
					<div className='col-xl-4'>
							<img
							className='d-block w-100'
							src={img5}
							/>
							<h5 className='text-center'>Description</h5>
					</div>
					<div className='col-xl-4'>
							<img
							className='d-block w-100'
							src={img6}
							/>
							<h5 className='text-center'>Description</h5>
					</div>
				</div>
						</Carousel.Item>
              </Carousel>
            </Card.Body>
          </Card>
        </Col>
			</div>	
			<div className='row'>
			<Col xl={12}>
          <Card>
            <Card.Body className='p-4'>
				<div className='row'>
					<div className='col'><h4 className='card-intro-title mb-4'>Employes on Highlight</h4></div>
					<div className='col'><h4 className='card-intro-title mb-4 text-end'><Link to={"./courses"} className="btn btn-primary btn-sm">View all</Link></h4></div>
				</div>
				<div className='row'>
				<div className="col-xl-4 col-xxl-5 col-lg-12">
					<div className="card instructors-box">
						<div className="card-body text-center pb-3">
							<div className="instructors-media">
								<img src={img2}  alt="" />
								<div className="instructors-media-info mt-4">
									<h4 className="mb-1">Nella Vita</h4>
									<span className="fs-18">Member Since 2020</span>
								</div>
							</div>
							<div className="bio text-center my-4">
								<h4 className="mb-3">Bio</h4>
								<div className="bio-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-xl-4 col-xxl-5 col-lg-12">
					<div className="card instructors-box">
						<div className="card-body text-center pb-3">
							<div className="instructors-media">
								<img src={img2}  alt="" />
								<div className="instructors-media-info mt-4">
									<h4 className="mb-1">Nella Vita</h4>
									<span className="fs-18">Member Since 2020</span>
								</div>
							</div>
							<div className="bio text-center my-4">
								<h4 className="mb-3">Bio</h4>
								<div className="bio-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-xl-4 col-xxl-5 col-lg-12">
					<div className="card instructors-box">
						<div className="card-body text-center pb-3">
							<div className="instructors-media">
								<img src={img2}  alt="" />
								<div className="instructors-media-info mt-4">
									<h4 className="mb-1">Nella Vita</h4>
									<span className="fs-18">Member Since 2020</span>
								</div>
							</div>
							<div className="bio text-center my-4">
								<h4 className="mb-3">Bio</h4>
								<div className="bio-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
            </Card.Body>
          </Card>
        </Col>
			</div>	
		</>
	)
}
export default Home;