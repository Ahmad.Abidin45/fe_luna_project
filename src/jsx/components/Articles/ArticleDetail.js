import React, { useState } from "react";
import { Link } from "react-router-dom";

/// Image
import profile08 from "./../../../images/ilustration/ilustrasi4.png";
import { Dropdown, Button, Modal } from "react-bootstrap";
import { SRLWrapper } from "simple-react-lightbox";

const ArticleDetail = () => {
  const [sendMessage, setSendMessage] = useState(false);

  const options = {
    settings: {
      overlayColor: "#000000",
    },
  };
  return (
    <div>
      <div>
        <div className="row">
          <div className="col-xl-12">
            <div className="card">
              <div className="card-body">
                <div className="post-details">
                  <h3 className="mb-2 text-black">
                    Collection of textile samples lay spread
                  </h3>

                  <ul className="mb-4 post-meta d-flex flex-wrap">
                    {/* <li className="post-author me-3">By Admin</li> */}
                    <li className="post-date me-3">
                      <i className="far fa-calendar-plus me-2" />
                      18 May 2022
                    </li>
                    {/* <li className="post-comment">
                      <i className="far fa-comment me-2" /> 28
                    </li> */}
                  </ul>

                  <img
                    src={profile08}
                    alt=""
                    className="img-fluid mb-3 w-100 rounded"
                  />
                  <p>
                    A wonderful serenity has take possession of my entire soul
                    like these sweet morning of spare which enjoy whole heart.A
                    wonderful serenity has take possession of my entire soul
                    like these sweet morning of spare which enjoy whole heart.
                  </p>
                  <p>
                    A collection of textile samples lay spread out on the table
                    - Samsa was a travelling salesman - and above it there hung
                    a picture that he had recently cut out of an illustrated
                    magazine and housed in a nice, gilded frame.
                  </p>
                  <blockquote>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Has been the industry's standard text
                    ever since the 1500s, when an unknown printer took a galley
                    of type and scrambled it to make a type specimencenturies.
                  </blockquote>
                  <p>
                    A wonderful serenity has taken possession of my entire soul,
                    like these sweet mornings of spring which I enjoy with my
                    whole heart. I am alone, and feel the charm of existence was
                    created for the bliss of souls like mine.I am so happy, my
                    dear friend, so absorbed in the exquisite sense of mere
                    tranquil existence, that I neglect my talents.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ArticleDetail;
