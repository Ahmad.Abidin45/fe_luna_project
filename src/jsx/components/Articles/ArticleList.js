import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";

//images
import palette from "./../../../images/svg/color-palette.svg";
import education from "./../../../images/svg/education-website.svg";
import brain from "./../../../images/svg/brain.svg";
import microscope from "./../../../images/svg/microscope.svg";
import course1 from "./../../../images/ilustration/ilustrasi1.png";
import course2 from "./../../../images/ilustration/ilustrasi2.png";
import course3 from "./../../../images/ilustration/ilustrasi3.png";
import course4 from "./../../../images/ilustration/ilustrasi4.png";
import course5 from "./../../../images/ilustration/ilustrasi1.png";
import course6 from "./../../../images/ilustration/ilustrasi2.png";

const widgetData = [
  { image: palette, title: "Graphic" },
  { image: education, title: "Coading" },
  { image: brain, title: "Soft Skill" },
  { image: microscope, title: "Science" },
];

const cardInfoBlog = [
  { title: "Fullstack Developer", subtitle: "Karen Hope ", image: course1 },
  { title: "UI Design Beginner", subtitle: "Jack and Sally", image: course2 },
  { title: "How to be Freelancer", subtitle: "Cahaya Hikari", image: course3 },
  { title: "UX Research", subtitle: "Johnny Ahmad", image: course4 },
  { title: "Basic Web Design", subtitle: "Jordan Nico", image: course5 },
  {
    title: "3D Character Design",
    subtitle: "Samantha William ",
    image: course6,
  },
];

function ArticleList() {
  let history = useHistory();

  const onClickDetail = () => {
    history.push("/article-detail");
  };

  return (
    <>
      <div className="widget-heading d-flex justify-content-between align-items-center">
        <h3 className="m-0">All Articles</h3>
        {/* <Link to={"./courses"} className="btn btn-primary btn-sm">
          View all
        </Link> */}
        <div class="input-group search-area" style={{ width: "20%" }}>
          <span class="input-group-text">
            <a href="#">
              <svg
                width="24"
                height="24"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M27.414 24.586L22.337 19.509C23.386 17.928 24 16.035 24 14C24 8.486 19.514 4 14 4C8.486 4 4 8.486 4 14C4 19.514 8.486 24 14 24C16.035 24 17.928 23.386 19.509 22.337L24.586 27.414C25.366 28.195 26.634 28.195 27.414 27.414C28.195 26.633 28.195 25.367 27.414 24.586ZM7 14C7 10.14 10.14 7 14 7C17.86 7 21 10.14 21 14C21 17.86 17.86 21 14 21C10.14 21 7 17.86 7 14Z"
                  fill="var(--secondary)"
                ></path>
              </svg>
            </a>
          </span>
          <input
            type="text"
            class="form-control"
            placeholder="Try article title"
          />
        </div>
      </div>
      <div className="row">
        {cardInfoBlog.map((data, index) => (
          <div className="col-xl-4 col-md-6" key={index}>
            <div
              className="card all-crs-wid"
              onClick={onClickDetail}
              style={{ cursor: "pointer" }}
            >
              <div className="card-body">
                <div className="courses-bx">
                  <div className="dlab-media">
                    <img src={data.image} alt="" />
                  </div>
                  <div className="dlab-info">
                    <div className="dlab-title d-flex justify-content-between">
                      <div>
                        <h4>
                          <Link to={"./course-details-1"}>{data.title}</Link>
                        </h4>
                        <p className="m-0">
                          {data.subtitle}
                          <svg
                            className="ms-1"
                            width="4"
                            height="5"
                            viewBox="0 0 4 5"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="2" cy="2.5" r="2" fill="#DBDBDB" />
                          </svg>
                        </p>
                      </div>
                      <h4 className="text-primary">
                        {/* <span>$</span>50.99 */}
                      </h4>
                    </div>
                    <div className="d-flex justify-content-between content align-items-center">
                      <li className="post-date me-3">
                        <i className="far fa-calendar-plus me-2" />
                        18 May 2022
                      </li>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="pagination-down">
        <div className="d-flex align-items-center justify-content-between flex-wrap">
          <h4 className="sm-mb-0 mb-3">
            Showing <span>1-6 </span>from <span>100 </span>data
          </h4>
          <ul>
            <li>
              <Link to={"#"}>
                <i className="fas fa-chevron-left"></i>
              </Link>
            </li>
            <li>
              <Link to={"#"} className="active">
                1
              </Link>
            </li>
            <li>
              <Link to={"#"}>2</Link>
            </li>
            <li>
              <Link to={"#"}>3</Link>
            </li>
            <li>
              <Link to={"#"}>
                <i className="fas fa-chevron-right"></i>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}
export default ArticleList;
